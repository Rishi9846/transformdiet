package com.transformdiet.utils;

/**
 * Created by Rishi Sharma on 6/10/2017.
 */
public class Constants {
    public static final int SALADS = 1;
    public static final int WRAPS = 2;
    public static final int MEALS = 3;
    public static final String OPTION = "potion";


    public static final String SALADS_URL = "http://transformdiet.in/salads/";
    public static final String WRAPS_URL = "http://transformdiet.in/wraps/";
    public static final String MEALS_URL = "http://transformdiet.in/meal/";
}
