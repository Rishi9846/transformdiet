package com.transformdiet.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.transformdiet.R;


public class ConnectionDetector {
    private Context context;

    public ConnectionDetector(Context context) {
        this.context = context;
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null
                    && activeNetwork.isConnected();

            return isConnected;
        }else
        showNoInternetDialog(context);
        return false;
    }

    public static void showNoInternetDialog(Context context) {

        new AlertDialog.Builder(context)
                .setCancelable(false)
                .setTitle(context.getString(R.string.internet))
                .setMessage(context.getString(R.string.check_internet))
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    public void showRetrofitErrorToast() {
        Toast.makeText(context, context.getString(R.string.might_problem), Toast.LENGTH_LONG).show();
    }

}
