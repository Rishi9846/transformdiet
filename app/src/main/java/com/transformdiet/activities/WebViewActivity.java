package com.transformdiet.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.transformdiet.R;
import com.transformdiet.utils.Constants;

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvTitle;
    private WebView webView;
    private ProgressWheel ivProgress;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        init();
        setData();
    }

    private void init() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        webView = (WebView) findViewById(R.id.webView);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivProgress = (ProgressWheel) findViewById(R.id.ivProgress);
        ivBack.setImageResource(R.drawable.back_white);
    }

    private void setData() {
        findViewById(R.id.ivRight).setVisibility(View.INVISIBLE);
        findViewById(R.id.ivBack).setOnClickListener(this);


        String url = "";
        switch (getIntent().getIntExtra(Constants.OPTION, 0)) {
            case Constants.SALADS:
                tvTitle.setText(getString(R.string.salads));
                url = Constants.SALADS_URL;
                break;
            case Constants.WRAPS:
                tvTitle.setText(getString(R.string.wraps));
                url = Constants.WRAPS_URL;
                break;
            case Constants.MEALS:
                tvTitle.setText(getString(R.string.meals));
                url = Constants.MEALS_URL;
                break;
        }


        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebChromeClient(new WebChromeClient());

        webView.loadUrl(url);

        try {

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    ivProgress.setVisibility(View.INVISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        finish();
    }

}
