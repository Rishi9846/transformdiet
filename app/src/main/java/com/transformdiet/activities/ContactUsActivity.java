package com.transformdiet.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.transformdiet.R;
import com.transformdiet.utils.ConnectionDetector;
import com.transformdiet.webservices.api.RestClient;
import com.transformdiet.webservices.pojos.PojoGeneral;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etFirstName, etPhone, etEmail, etContact;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        init();
    }

    private void init() {
        progressDialog = new ProgressDialog(this);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etContact = (EditText) findViewById(R.id.etContact);

        findViewById(R.id.btnSubmit).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (checkValidation())
            contactUsApi();
    }

    private boolean checkValidation() {
        if (etFirstName.getText().toString().isEmpty()) {
            etFirstName.requestFocus();
            etFirstName.setError(getString(R.string.enter_name));
            return false;
        } else if (etEmail.getText().toString().isEmpty()) {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.enter_email));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(
                etEmail.getText().toString()).matches()) {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.enter_valid_email));
            return false;
        } else if (etPhone.getText().toString().length() < 8) {
            etPhone.requestFocus();
            etPhone.setError(getString(R.string.enter_phone));
            return false;
        } else if (etContact.getText().toString().isEmpty()) {
            etContact.requestFocus();
            etContact.setError(getString(R.string.enter_contact));
            return false;
        }
        return true;
    }

    private void contactUsApi() {
        if (ConnectionDetector.isConnectingToInternet(this)) {
            progressDialog.show();

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("fn", etFirstName.getText().toString().trim());
            hashMap.put("email", etEmail.getText().toString().trim());
            hashMap.put("ph", etPhone.getText().toString().trim());
            hashMap.put("cc", etContact.getText().toString().trim());

            RestClient.get().contactUs(hashMap).enqueue(new Callback<PojoGeneral>() {
                @Override
                public void onResponse(Call<PojoGeneral> call, Response<PojoGeneral> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body().status == 201) {
                            Toast.makeText(ContactUsActivity.this, getString(R.string.success),
                                    Toast.LENGTH_LONG).show();
                            finish();
                        } else
                            Toast.makeText(ContactUsActivity.this, getString(R.string.some_wrong),
                                    Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<PojoGeneral> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } else
            new ConnectionDetector(this).showNoInternetDialog(this);
    }

}
