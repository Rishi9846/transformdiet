package com.transformdiet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.transformdiet.R;
import com.transformdiet.utils.Constants;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setViews();
    }

    private void setViews() {
        findViewById(R.id.ivBack).setOnClickListener(this);
        findViewById(R.id.ivRight).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlSalads:
            case R.id.btnSalads:
                setIntent(Constants.SALADS);
                break;
            case R.id.rlWraps:
            case R.id.btnWraps:
                setIntent(Constants.WRAPS);
                break;
            case R.id.rlMeals:
            case R.id.btnMeals:
                setIntent(Constants.MEALS);
                break;
            case R.id.ivBack:
                startActivity(new Intent(this, ContactUsActivity.class));
                break;
            case R.id.ivRight:
                shareLink();
                break;

        }
    }

    private void shareLink() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out my app at: https://play.google.com/store/apps/details?id=" +
                        getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void setIntent(int option) {
        startActivity(new Intent(this, WebViewActivity.class).
                putExtra(Constants.OPTION, option));
    }
}
