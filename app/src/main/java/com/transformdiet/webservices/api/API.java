package com.transformdiet.webservices.api;

import com.transformdiet.webservices.pojos.PojoGeneral;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by cbl29 on 21/7/16.
 */
public interface API {

    @FormUrlEncoded
    @POST("contact-api.php")
    Call<PojoGeneral> contactUs(@FieldMap HashMap<String, String> hashMap);

}